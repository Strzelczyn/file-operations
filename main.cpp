#include <fstream>
#include <iostream>
#include <memory>

int main() {
  std::fstream file;
  std::unique_ptr<std::string> firstLine(new std::string("First line."));
  file.open("store.txt", std::ios::app | std::ios::out);
  if (file.good() == true) {
    file << *firstLine << std::endl;
    file.close();
  }
  std::unique_ptr<std::string> nextLine(new std::string("Next line."));
  file.open("store.txt", std::ios::app | std::ios::out);
  if (file.good() == true) {
    file << *nextLine << std::endl;
    file.close();
  }
  std::unique_ptr<std::string> readLine(new std::string);
  file.open("store.txt", std::ios::in | std::ios::out);
  if (file.good() == true) {
    while (std::getline(file, *readLine)) {
      std::cout << *readLine << '\n';
    }
    file.close();
  }
}
